package com.example.demo.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.example.demo.validator.CheckCorrelationValidator;

@Constraint(validatedBy = {CheckCorrelationValidator.class})
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CheckCorrelation {

	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	//エラー時に例外オブジェクトに設定されるメッセージ
	String message() default "支店と部署の組み合わせが間違っています";;

	@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@Documented
	public @interface List {
		CheckCorrelation[] value();
	}
}
