package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer>{
	@Query("select t from Message t where t.created_date between ?1 and ?2 and t.category like %?3%")
	List<Message> findByDate(Timestamp start, Timestamp end, String category);

	@Query("select t from Message t order by t.created_date desc")
	List<Message> findByOrderByCreatedDateDesc();
}
