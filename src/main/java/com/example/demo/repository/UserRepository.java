package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	@Query("select t from User t where t.account = ?1 AND t.password = ?2")
	User findByUser(String account, String password);

	@Query("select t from User t where t.account = ?1")
	User checkAccount(String account);

	@Query("select t from User t where t.id = ?1")
	User findbyUser(int id);

	@Query("select t from User t order by t.id asc")
	List<User> findByOrderByIdAsc();
}
