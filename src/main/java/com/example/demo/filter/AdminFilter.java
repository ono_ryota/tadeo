package com.example.demo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.entity.User;


public class AdminFilter implements Filter{

	@Autowired
	HttpSession session;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("AdminFilterのdoFilter実行");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		User loginUser = (User) session.getAttribute("loginUser");

		// 管理者でない場合は、TOPページへ遷移する
	if (loginUser.getDepartment_id() != 1) {
			session.setAttribute("adminErrors", "管理者権限がありません");

			res.sendRedirect("/");
			return;
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig config){
		System.out.println("AdminFilterのinit実行");
	}

	@Override
	public void destroy() {
		System.out.println("AdminFilterのdestroy実行");
	}
}
