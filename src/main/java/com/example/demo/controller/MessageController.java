package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.UserService;

@Controller
public class MessageController{

	@Autowired
	UserService userService;

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	HttpSession session;

	// 投稿表示画面
	@GetMapping("/Tadeo")
	public ModelAndView index(){
		ModelAndView mav = new ModelAndView();
		// 投稿の取得
		List<Message> messageData = messageService.findAllMessage();
		// コメントform用の空のentityを準備
		Comment comment = new Comment();
		// ユーザーリストの取得
		List<User> userData = userService.findAllUser();
		// コメントの取得
		List<Comment> commentData = commentService.findAllComment();
		// ログインUserの取得
		User user = (User) session.getAttribute("loginUser");
		// 画面遷移先の指定
		mav.setViewName("/message/top");
		// オブジェクトを保管
		mav.addObject("users", userData);
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		mav.addObject("formModel", comment);
		mav.addObject("loginUser", user);

		return mav;
	}

	// トップ画面
	@GetMapping({ "/", "/top" })
	public ModelAndView top(){
		ModelAndView mav = new ModelAndView();
		// 投稿の取得
		List<Message> messageData = messageService.findAllMessage();
		// コメントform用の空のentityを準備
		Comment comment = new Comment();
		// ユーザーリストの取得
		List<User> userData = userService.findAllUser();
		// コメントの取得
		List<Comment> commentData = commentService.findAllComment();
		// ログインUserの取得
		User user = (User) session.getAttribute("loginUser");
		// オブジェクトを保管
		mav.addObject("users", userData);
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		mav.addObject("formModel", comment);
		mav.addObject("loginUser", user);

		return new ModelAndView("redirect:/Tadeo");
	}

	// 新規投稿画面
	@GetMapping("/message/new")
	public ModelAndView newContent(@ModelAttribute("formModel") Message messageFailure){
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Message message = new Message();
		// ログイン中のUserを取得
		User loginUser = (User) session.getAttribute("loginUser");
		// entityにuser_idをセット
		message.setUser_id(loginUser.getId());
		mav.addObject("loginUser", loginUser);
		// 画面遷移先の指定
		mav.setViewName("/message/new");
		if(messageFailure.getText() == null) {
			mav.addObject("formModel", message);
		}

		return mav;
	}

	// 投稿処理
	@PostMapping("/message/add")
	public ModelAndView addContent(@Validated @ModelAttribute("formModel") Message message, BindingResult result,
			RedirectAttributes attributes){
		if(result.hasErrors()){
			List<String> errorMessages = new ArrayList<>();
			for(ObjectError error : result.getAllErrors()){
				errorMessages.add(error.getDefaultMessage());
			}
			attributes.addFlashAttribute("errorMessages", errorMessages);
			attributes.addFlashAttribute("formModel", message);

			return new ModelAndView("redirect:/message/new");
		}

		// 投稿をテーブルに格納
		messageService.saveMessage(message);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("message/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id){
		// 投稿をテーブルから削除
		messageService.deleteMessage(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 絞込処理
	@GetMapping("search")
	public ModelAndView searchContent(@RequestParam("start") String start, @RequestParam("end") String end,
			@RequestParam("category") String category){
		ModelAndView mav = new ModelAndView();
		// 投稿の絞込
		List<Message> messageData = messageService.searchMessage(start, end, category);
		// コメントform用の空のentityを準備
		Comment comment = new Comment();
		// コメントの取得
		List<Comment> commentData = commentService.findAllComment();
		// ログイン中のUserを取得
		User loginUser = (User) session.getAttribute("loginUser");
		// 画面遷移先を指定
		mav.setViewName("/message/top");
		// オブジェクトを保管
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		mav.addObject("loginUser", loginUser);
		mav.addObject("formModel", comment);
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.addObject("category", category);

		return mav;

	}
}
