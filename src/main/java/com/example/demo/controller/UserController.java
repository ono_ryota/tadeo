package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.LoginUser;
import com.example.demo.entity.User;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@Controller
public class UserController{

	@Autowired
	UserService userService;

	@Autowired
	BranchService branchService;

	@Autowired
	DepartmentService departmentService;

	@Autowired
	HttpSession session;

	//新規登録画面
	@GetMapping("user/new")
	public ModelAndView newUser(@ModelAttribute("formModel") User userFailure){
		ModelAndView mav = new ModelAndView();
		//form用の空entity
		User user = new User();
		//支店・部署を全件取得
		List<Branch> branches = branchService.findAllBranch();
		List<Department> departments = departmentService.findAllDepartment();
		// ログイン中のUserを取得
		User loginUser = (User) session.getAttribute("loginUser");

		// 画面遷移先の指定
		mav.setViewName("/user/new");
		// オブジェクトを保管
		if(userFailure == null){
			mav.addObject("formModel", user);
		}
		mav.addObject("branches", branches);
		mav.addObject("departments", departments);
		mav.addObject("loginUser", loginUser);

		return mav;
	}

	//新規ユーザー作成処理
	@PostMapping("user/add")
	public ModelAndView addUser(@Validated @ModelAttribute User user, BindingResult error,
			RedirectAttributes redirectAttributes){
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		String password = user.getPassword();
		String confirmPassword = user.getConfirmPassword();
		String account = user.getAccount();

		//アカウントが重複してるか確認するための条件分岐
		if(userService.checkDuplicate(account) != null){
			FieldError duplicateError = new FieldError(error.getObjectName(), "account", "アカウントが重複しています");
			error.addError(duplicateError);
		}

		//パスワードと確認用パスワードが一致しているかを条件分岐で確認
		if(!userService.checkPassword(password, confirmPassword)){
			// フィールドのエラーを表し、Formクラス名，フィールド名，エラーメッセージを渡す。
			// getObjectNameではフォームクラス名が取得できる
			FieldError passwordError = new FieldError(error.getObjectName(), "password", "パスワードと確認用パスワードが一致しません");

			//エラーを追加する
			error.addError(passwordError);
		}

		//バリデーションチェックに引っかかった場合は入力画面を再度表示
		if(error.hasErrors()){
			for(ObjectError result : error.getAllErrors()){
				errorMessages.add(result.getDefaultMessage());
			}

			// パスワードが空の場合のエラー処理
			if(ObjectUtils.isEmpty(password)){
				errorMessages.add("パスワードを入力してください");
			}else if(password.length() < 6){
				errorMessages.add("パスワードは6文字以上で入力してください");
			}else if(password.length() > 20){
				errorMessages.add("パスワードは20文字以下で入力してください");
			}

			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			redirectAttributes.addFlashAttribute("formModel", user);

			return new ModelAndView("redirect:/user/new");
		}else if(ObjectUtils.isEmpty(password)){
			errorMessages.add("パスワードを入力してください");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			redirectAttributes.addFlashAttribute("formModel", user);

			return new ModelAndView("redirect:/user/new");
		}else if(password.length() < 6){
			errorMessages.add("パスワードは6文字以上で入力してください");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			redirectAttributes.addFlashAttribute("formModel", user);

			return new ModelAndView("redirect:/user/new");
		}else if(password.length() > 20){
			errorMessages.add("パスワードは20文字以下で入力してください");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			redirectAttributes.addFlashAttribute("formModel", user);

			return new ModelAndView("redirect:/user/new");
		}

		//ユーザーをテーブルに格納
		userService.saveUser(user);

		//rootへリダイレクト
		return new ModelAndView("redirect:/user/index");
	}

	//ユーザー一覧画面
	@GetMapping("user/index")
	public ModelAndView indexUser(){
		ModelAndView mav = new ModelAndView();
		//ユーザーを全件取得
		List<User> userData = userService.findAllUser();
		//支店・部署を全件取得
		List<Branch> branches = branchService.findAllBranch();
		List<Department> departments = departmentService.findAllDepartment();
		// ログイン中のUserを取得
		User loginUser = (User) session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);
		mav.addObject("branches", branches);
		mav.addObject("departments", departments);
		mav.setViewName("user/index");
		mav.addObject("users", userData);
		return mav;
	}

	//ユーザー編集画面
	@GetMapping("user/edit/{id}")
	public ModelAndView editUser(@PathVariable String id, RedirectAttributes redirectAttributes){
		ModelAndView mav = new ModelAndView();

		if(!id.isEmpty() && id.matches("^[0-9]*$") && userService.findById(Integer.parseInt(id)) != null){
			User user = userService.findById(Integer.parseInt(id));
			mav.setViewName("user/edit");
			mav.addObject("formModel", user);
		}else{
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("不正なパラメーターが入力されました");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			return new ModelAndView("redirect:/Tadeo");
		}
		// ログイン中のUserを取得
		User loginUser = (User) session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		return mav;
	}

	@GetMapping("user/edit")
	public ModelAndView parameterError(RedirectAttributes redirectAttributes){
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメーターが入力されました");
		redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
		return new ModelAndView("redirect:/Tadeo");
	}

	//ユーザー情報編集処理
	@PutMapping("user/update/{id}")
	public ModelAndView updateUser(@PathVariable Integer id, @Validated @ModelAttribute("formModel") User user,
			BindingResult error, RedirectAttributes redirectAttributes, HttpServletRequest request){
		ModelAndView mav = new ModelAndView();

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		//ログインユーザーが自分自身の支社と部署を変更しようとしたときの処理
		if(loginUser.getId() == user.getId() && user.getDepartment_id() != 1){
			mav.setViewName("user/edit");
			return mav;
		}

		String password = user.getPassword();
		String confirmPassword = user.getConfirmPassword();
		String account = user.getAccount();

		User checkUser = userService.checkDuplicate(account);
		//アカウントが重複してるか確認するための条件分岐
		if(checkUser != null && checkUser.getId() != id){
			FieldError duplicateError = new FieldError(error.getObjectName(), "account", "アカウントが重複しています");
			error.addError(duplicateError);
		}
		//パスワードと確認用パスワードが一致しているかを条件分岐で確認
		if(!userService.checkPassword(password, confirmPassword)){
			// フィールドのエラーを表し、Formクラス名，フィールド名，エラーメッセージを渡す。
			// getObjectNameではフォームクラス名が取得できる
			FieldError passwordError = new FieldError(error.getObjectName(), "password", "パスワードと確認用パスワードが一致しません");

			//エラーを追加する
			error.addError(passwordError);
		}

		//バリデーションチェックに引っかかった場合は入力画面を再度表示
		List<String> errorMessages = new ArrayList<String>();
		if(error.hasErrors()){
			for(ObjectError result : error.getAllErrors()){
				errorMessages.add(result.getDefaultMessage());
			}

			// パスワードエラー時の処理
			if(!ObjectUtils.isEmpty(password)){
				if(password.length() < 6){
					errorMessages.add("パスワードは6文字以上で入力してください");
				}else if(password.length() > 20){
					errorMessages.add("パスワードは20文字以下で入力してください");
				}
			}

			mav.addObject("errorMessages", errorMessages);
			mav.addObject("formModel", user);
			mav.setViewName("user/edit");
			mav.addObject("loginUser", loginUser);
			return mav;
		}else if(!ObjectUtils.isEmpty(password)){
			if(password.length() < 6){
				errorMessages.add("パスワードは6文字以上で入力してください");

				redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
				return new ModelAndView("redirect:/user/edit/{id}");
			}else if(password.length() > 20){
				errorMessages.add("パスワードは20文字以下で入力してください");

				redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
				return new ModelAndView("redirect:/user/edit/{id}");
			}
		}
		user.setId(id);
		userService.saveUser(user);
		return new ModelAndView("redirect:/user/index");
	}

	//ユーザー退会処理
	@PutMapping("user/delete/{id}")
	public ModelAndView deleteUser(@PathVariable Integer id, ServletRequest request){

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();

		User loginUser = (User) session.getAttribute("loginUser");

		if(id == loginUser.getId()){
			return new ModelAndView("redirect:/user/index");
		}

		ModelAndView mav = new ModelAndView();
		User user = userService.findById(id);
		if(user.isIs_stopped() == false){
			user.setIs_stopped(true);
			userService.saveUser(user);
		}else{
			user.setIs_stopped(false);
			userService.saveUser(user);
		}
		//ユーザーを全件取得
		List<User> userData = userService.findAllUser();
		//支店・部署を全件取得
		List<Branch> branches = branchService.findAllBranch();
		List<Department> departments = departmentService.findAllDepartment();

		mav.addObject("loginUser", loginUser);
		mav.addObject("branches", branches);
		mav.addObject("departments", departments);
		mav.addObject("users", userData);
		mav.setViewName("user/index");
		return mav;
	}

	// ログインページ
	@GetMapping("/login")
	public ModelAndView loginUser(@ModelAttribute("formModel") LoginUser loginFailure){
		ModelAndView mav = new ModelAndView();
		//form用の空entity
		LoginUser loginUser = new LoginUser();
		mav.setViewName("/user/login");
		if(loginFailure == null){
			mav.addObject("formModel", loginUser);
		}
		return mav;
	}

	// ログイン処理
	@PostMapping("/login")
	public ModelAndView loginProcess(
			@Validated @ModelAttribute("formModel") LoginUser loginUser,
			BindingResult result, RedirectAttributes redirectAttributes){

		List<String> errorMessages = new ArrayList<String>();

		// accountとpasswordが入力されていない場合のバリデーション
		if(result.hasErrors()){
			for(ObjectError error : result.getAllErrors()){
				errorMessages.add(error.getDefaultMessage());
			}

			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);

			return new ModelAndView("redirect:/login");
		}

		// accountとpasswordでユーザーを取得
		User user = userService.findByUser(loginUser.getAccount(), loginUser.getPassword());

		// Userテーブルに登録されていない場合のエラーメッセージ
		if(user == null){
			errorMessages.add("アカウントまたはパスワードが誤っています");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			redirectAttributes.addFlashAttribute("formModel", loginUser);

			return new ModelAndView("redirect:/login");
		}

		// 停止状態のaccountでログインする場合のエラーメッセージ
		if(user.isIs_stopped() == true){
			errorMessages.add("アカウントまたはパスワードが誤っています");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);

			return new ModelAndView("redirect:/login");
		}

		session.setAttribute("loginUser", user);
		return new ModelAndView("redirect:/");
	}

	// ログアウト処理
	@GetMapping("/logout")
	public ModelAndView logout(){
		session.invalidate();
		return new ModelAndView("redirect:/login");
	}
}