package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	@Autowired
	MessageService messageService;

	// コメント投稿処理
	@PostMapping("/comment/add")
	public String addComment(@Validated @ModelAttribute("formModel") Comment comment, BindingResult result, RedirectAttributes attributes) {
		if(result.hasErrors()) {
			List<String> errorMessages = new ArrayList<String>();
			for (ObjectError error : result.getAllErrors())  {
				errorMessages.add(error.getDefaultMessage());
			}
			attributes.addFlashAttribute("errorMessages", errorMessages);

			return "redirect:/Tadeo";
		}
		// コメントをテーブルに格納
		commentService.saveComment(comment);

		return "redirect:/";
	}

	// 削除処理
	@DeleteMapping("/comment/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		// コメントをテーブルから削除
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}
