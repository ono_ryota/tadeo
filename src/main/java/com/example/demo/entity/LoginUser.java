package com.example.demo.entity;

import javax.validation.constraints.NotBlank;

public class LoginUser {

	@NotBlank(message="アカウントが入力されていません")
	private String account;

	@NotBlank(message="パスワードが入力されていません")
	private String password;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
