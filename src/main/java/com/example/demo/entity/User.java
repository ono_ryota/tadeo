package com.example.demo.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.example.demo.annotation.CheckCorrelation;

@Entity
@Table(name = "users")
@CheckCorrelation
public class User {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	@Pattern(regexp = "^[0-9a-zA-Z]*$", message="アカウントは半角英数字で入力してください")
	@NotBlank(message = "アカウントを入力してください")
	@Size(min=6, message="アカウント名は6文字以上で入力してください")
	@Size(max=20, message="アカウント名は20文字以下で入力してください")
	private String account;


	@Column
	private String password;

	@Transient
	private String confirmPassword;

	@Column
	@NotBlank(message = "名前を入力してください")
	@Size(max=10, message="名前は10文字以内で入力してください")
	private String name;

	@Column
	private int branch_id;

	@Column
	private int department_id;

	@Column
	private boolean is_stopped;

	@CreationTimestamp
	@Column(updatable = false)
	private Timestamp created_date;

	@UpdateTimestamp
	@Column
	private Timestamp updated_date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(Integer branch_id) {
		this.branch_id = branch_id;
	}

	public Integer getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(Integer department_id) {
		this.department_id = department_id;
	}

	public boolean isIs_stopped() {
		return is_stopped;
	}

	public void setIs_stopped(boolean is_stopped) {
		this.is_stopped = is_stopped;
	}

	public Timestamp getCreate_date() {
		return created_date;
	}

	public void setCreate_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public Timestamp getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}

}
