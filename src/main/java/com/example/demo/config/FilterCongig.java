package com.example.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.filter.AdminFilter;
import com.example.demo.filter.LoginFilter;

@Configuration
public class FilterCongig {

	//ログインフィルターのURLパターンを設定する処理
	@Bean
	public FilterRegistrationBean loginFilter1() {
		//ログインフィルターを１番目に実行するフィルタとして追加
		FilterRegistrationBean<LoginFilter> bean = new FilterRegistrationBean<LoginFilter>(new LoginFilter());

		//フィルターをかけるURLを指定
		bean.addUrlPatterns("/Tadeo","/message/top", "/message/new", "/user/new", "/user/edit/*", "/user/index");

		//ログインフィルターの実行順序を１番目に設定
		bean.setOrder(1);
		return bean;
	}

	//管理者フィルターのURLパターンを設定する処理
		@Bean
		public FilterRegistrationBean adminFilter1() {
			//ログインフィルターを2番目に実行するフィルタとして追加
			FilterRegistrationBean bean = new FilterRegistrationBean(new AdminFilter());

			//フィルターをかけるURLを指定
			bean.addUrlPatterns("/user/new", "/user/edit/*", "/user/index");

			//管理者フィルターの実行順序を2番目に設定
			bean.setOrder(2);
			return bean;
		}

}
