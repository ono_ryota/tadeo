package com.example.demo.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.ObjectUtils;

import com.example.demo.annotation.CheckCorrelation;
import com.example.demo.entity.User;

public class CheckCorrelationValidator implements ConstraintValidator<CheckCorrelation, Object>{

	private String branch_id;
	private String department_id;


	//初期化処理
	//アノテーションの情報を受け取る
	@Override
	public void initialize(CheckCorrelation annotation) {
		//アノテーションの引数情報を設定する
		this.branch_id = "branch_id";
		this.department_id = "department_id";
	}

	//比較処理
	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext context) {
		BeanWrapper beanWrapper = new BeanWrapperImpl(obj);
		Object branch = beanWrapper.getPropertyValue(branch_id);
		Object department = beanWrapper.getPropertyValue(department_id);


		if(branch.equals(1) && (department.equals(1) || department.equals(2))) {
			return true;
		}

		if((branch.equals(2) || branch.equals(3) || branch.equals(4)) && (department.equals(3) || department.equals(4))) {
			return true;
		}

		return false;
	}
}
