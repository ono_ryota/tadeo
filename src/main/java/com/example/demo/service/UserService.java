package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.security.CipherUtil;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	//ユーザー情報を登録・更新するメソッド
	public void saveUser(User user) {
		if(user.getPassword() != null) {
			user.setPassword(CipherUtil.encrypt(user.getPassword()));
		}
		userRepository.save(user);
	}

	//idが一致するユーザーを取得するメソッド
	public User findById(Integer id) {
		return userRepository.findbyUser(id);
	}

	//ユーザー全件取得するメソッド
	public List<User> findAllUser() {
		return userRepository.findByOrderByIdAsc();
	}

	// accountとpasswordが一致するユーザーを取得する
	public User findByUser(String account, String password) {
		password = CipherUtil.encrypt(password);
		return userRepository.findByUser(account, password);
	}

	//パスワードと確認用パスワードが一致しているかチェックするメソッド
	public boolean checkPassword(String password, String confirmPassword) {
		if(password.equals(confirmPassword)) {
			return true;
		}

		return false;
	}
	//アカウントにダブりがないか確認するメソッド
	public User checkDuplicate(String account) {
		return userRepository.checkAccount(account);
	}
}
