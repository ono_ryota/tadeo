package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessageService{

	@Autowired
	MessageRepository messageRepository;

	// レコードを全件取得
	public List<Message> findAllMessage(){
		return messageRepository.findByOrderByCreatedDateDesc();
	}

	// レコード追加
	public void saveMessage(Message message){
		messageRepository.save(message);
	}

	// レコード削除
	public void deleteMessage(Integer id){
		messageRepository.deleteById(id);
	}

	// レコードの絞込
	public List<Message> searchMessage(String start, String end, String category) {
		// 現在の時刻を取得
		Date date = new Date();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String defaultEnd = dateformat.format(date);

		if(StringUtils.hasText(start)) {
			start = start + " 00:00:00";
		} else {
			start = "2020-08-01 00:00:00";
		}

		if(StringUtils.hasText(end)) {
			end = end + " 23:59:59";
		} else {
			end = defaultEnd;
		}

		Timestamp startData = Timestamp.valueOf(start);
		Timestamp endData = Timestamp.valueOf(end);

		List<Message> messages = messageRepository.findByDate(startData, endData, category);
		return messages;
	}
}
