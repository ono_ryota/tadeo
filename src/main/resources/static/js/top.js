$(function() {

	const DELETE_MESSAGE = "投稿を削除します。よろしいですか？"
	const DELETE_COMMENT = "コメントを削除します。よろしいですか？"

	/*投稿削除処理*/
	$('.delete-message').on('click', function() {
		if(!confirm(DELETE_MESSAGE)) {
			return false;
		}
	});

	/*コメント削除処理*/
	$('.delete-comment').on('click', function() {
		if(!confirm(DELETE_COMMENT)) {
			return false;
		}
	});
});