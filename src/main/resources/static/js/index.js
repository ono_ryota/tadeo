$(function() {

	const DELETE_MESSAGE = "退会してもよろしいでしょうか。"
	const RECOVERY_MESSAGE = "アカウントを復活させます。よろしいですか？"
	$('.delete').on('click', function() {
		if(!confirm(DELETE_MESSAGE)) {
			return false;
		}
	});

	$('.recovery').on('click', function() {
		if(!confirm(RECOVERY_MESSAGE)) {
			return false;
		}
	});
});